#!/bin/bash
echo "__CHANGE ROOT PASSWORD__"
passwd

echo "__FIX FEDORA REPOS__"
mv /etc/yum.repos.d/fedora.repo /etc/yum.repos.d/fedora.repo.bak
mv /etc/yum.repos.d/fedora-updates.repo /etc/yum.repos.d/fedora-updates.repo.bak
mv /etc/yum.repos.d/fedora-updates-testing.repo /etc/yum.repos.d/fedora-updates-testing.repo.bak
echo "[fedora]
name=Fedora $releasever - $basearch
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/releases/$releasever/Everything/$basearch/os/
metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch
enabled=1
metadata_expire=7d
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False

[fedora-debuginfo]
name=Fedora $releasever - $basearch - Debug
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/releases/$releasever/Everything/$basearch/debug/
metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-debug-$releasever&arch=$basearch
enabled=0
metadata_expire=7d
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False

[fedora-source]
name=Fedora $releasever - Source
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/releases/$releasever/Everything/source/SRPMS/
metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-source-$releasever&arch=$basearch
enabled=0
metadata_expire=7d
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False" > /etc/yum.repos.d/fedora.repo

echo "[updates]
name=Fedora $releasever - $basearch - Updates
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/$releasever/$basearch/
metalink=https://mirrors.fedoraproject.org/metalink?repo=updates-released-f$releasever&arch=$basearch
enabled=1
metadata_expire=6h
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False

[updates-debuginfo]
name=Fedora $releasever - $basearch - Updates - Debug
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/$releasever/$basearch/debug/
metalink=https://mirrors.fedoraproject.org/metalink?repo=updates-released-debug-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
metadata_expire=6h
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False

[updates-source]
name=Fedora $releasever - Updates Source
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/$releasever/SRPMS/
metalink=https://mirrors.fedoraproject.org/metalink?repo=updates-released-source-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
metadata_expire=6h
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False" > /etc/yum.repos.d/fedora-updates.repo

echo "[updates-testing]
name=Fedora $releasever - $basearch - Test Updates
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/$basearch/
metalink=https://mirrors.fedoraproject.org/metalink?repo=updates-testing-f$releasever&arch=$basearch
enabled=0
metadata_expire=6h
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False

[updates-testing-debuginfo]
name=Fedora $releasever - $basearch - Test Updates Debug
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/$basearch/debug/
metalink=https://mirrors.fedoraproject.org/metalink?repo=updates-testing-debug-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
metadata_expire=6h
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False

[updates-testing-source]
name=Fedora $releasever - Test Updates Source
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/SRPMS/
metalink=https://mirrors.fedoraproject.org/metalink?repo=updates-testing-source-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
metadata_expire=6h
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False" > /etc/yum.repos.d/fedora-updates-testing.repo

echo "__INSTALLING IPTABLES__"
yum install iptables-services -y

echo "__INSTALLING MOD-SECURITY__"
yum install mod_security -y

echo "__SHUTTING DOWN FIREWALLD__"
systemctl status firewalld
systemctl disable firewalld
systemctl mask firewalld

echo "__STARTING IPTABLES__"
systemctl start iptables
systemctl enable iptables

echo "__SETTING UP IPTABLES__"
echo "__SAVING IP IPTABLES TO /root/iptables_setup.sh__"
echo "#!/bin/bash
iptables -F # (flush rules)

# Default drop action
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

# Allow outbound DNS
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --sport 53 -j ACCEPT

# Allow POP3
iptables -A INPUT -p tcp --dport 110 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 110 -m state --state ESTABLISHED -j ACCEPT

# Allow POP3S
#iptables -A INPUT -p tcp --dport 995 -m state --state NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 995 -m state --state ESTABLISHED -j ACCEPT

# SMTP
iptables -A INPUT -p tcp -m tcp --dport 25 -j ACCEPT
#iptables -A INPUT -p tcp -m tcp --dport 2525 -j ACCEPT
#iptables -A INPUT -p tcp -m tcp --dport 587 -j ACCEPT
#iptables -A INPUT -p tcp -m tcp --dport 465 -j ACCEPT

# Allow IMAP
#iptables -A INPUT -p tcp --dport 143 -m state --state NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 143 -m state --state ESTABLISHED -j ACCEPT

# Allow IMAPS
#iptables -A INPUT -p tcp --dport 993 -m state --state NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 993 -m state --state ESTABLISHED -j ACCEPT

# Allow Sendmail or Postfix
iptables -A INPUT -p tcp --dport 25 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 25 -m state --state ESTABLISHED -j ACCEPT

# Allow rsync from a specific network
#iptables -A INPUT -i eth0 -p tcp -s 192.168.101.0/24 --dport 873 -m state --state #NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -o eth0 -p tcp --sport 873 -m state --state ESTABLISHED -j ACCEPT

# These rules add scanners to the portscan list, and log the attempt.
iptables -A INPUT   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
iptables -A INPUT   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP

iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP

# Anti DoS attack
iptables -I INPUT -p tcp --dport 80 -m connlimit --connlimit-above 20 --connlimit-mask 40 -j DROP

# Anti script kiddy
iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP

# Anti rcon packets
iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP

# anti xmas 
iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP

# Ping from inside to outside
#iptables -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT
#iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT

# yum stuff
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp -m tcp --dport 443 -j ACCEPT

iptables-save > /etc/sysconfig/iptables" > /root/iptables_setup.sh

echo "__CHANGING PERMISSIONS ON IPTABLES SCRIPT__"
chmod u+x /root/iptables_setup.sh

echo "__EXECUTING IPTABLES__"
./iptables_setup.sh

echo "__IPTABLES LIST__"
iptables -L

echo "__SHUTTING DOWN SERVICES__"
echo "__MAKING SHUTDOWN SCRIPT__"
echo "#!/bin/bash
systemctl stop abrt-ccpp.service
systemctl disable abrt-ccpp.service
systemctl mask abrt-ccpp.service

systemctl stop abrt-oops.service
systemctl disable abrt-oops.service
systemctl mask abrt-oops.service

systemctl stop abrt-vmcore.service
systemctl disable abrt-vmcore.service
systemctl mask abrt-vmcore.service

systemctl stop abrt-xorg.service
systemctl disable abrt-xorg.service
systemctl mask abrt-xorg.service

systemctl stop bluetooth.target
systemctl disable bluetooth.target
systemctl mask bluetooth.target

systemctl stop abrtd.service
systemctl disable abrtd.service
systemctl mask abrtd.service

systemctl stop graphical.target
systemctl disable graphical.target
systemctl mask graphical.target

systemctl stop dbus-org.fedoraproject.FirewallD1.service
systemctl disable dbus-org.fedoraproject.FirewallD1.service
systemctl mask dbus-org.fedoraproject.FirewallD1.service

systemctl stop fprintd.service
systemctl disable fprintd.service
systemctl mask fprintd.service

systemctl stop smartd.service
systemctl disable smartd.service
systemctl mask smartd.service

systemctl stop sshd.service
systemctl disable sshd.service
systemctl mask sshd.service

systemctl stop sshd-keygen.service
systemctl disable sshd-keygen.service
systemctl mask sshd-keygen.service

systemctl stop sshd@.service
systemctl disable sshd@.service
systemctl mask sshd@.service

systemctl stop cockpit.socket
systemctl disable cockpit.socket
systemctl mask cockpit.socket

systemctl stop cockpit.service
systemctl disable cockpit.service
systemctl mask cockpit.service

systemctl stop pcscd.service
systemctl disable pcscd.service
systemctl mask pcscd.service

systemctl stop pcscd.socket
systemctl disable pcscd.socket
systemctl mask pcscd.socke

systemctl stop  nfs-client.target
systemctl disable nfs-client.target
systemctl mask nfs-client.target

systemctl stop Rpcbind.socket
systemctl disable rcpbind.socket
systemctl mask rcpbind.socket

systemctl stop remote-fs.target
systemctl disable remote-fs.target
systemctl mask remote-fs.target" > /root/shutdown_script.sh

echo "__CHANGING PERMISSIONS ON SHUTDOWN SCRIPT__"
chmod u+x /root/shutdown_script.sh

echo "__EXECUTING IPTABLES__"
./shutdown_script.sh

echo "__CREATING SCRIPT TO DISABLE IPv6__"
echo "# Disable
sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1

# Enable
# sysctl -w net.ipv6.conf.all.disable_ipv6=0
# sysctl -w net.ipv6.conf.default.disable_ipv6=0" > /root/disable_ipv6.sh

echo "__MAKING DISABLE IPV6 SCRIPT__"
chmod u+x /root/disable_ipv6.sh

echo "__RUNNING DISABLE IPV6 SCRIPT__"
./disable_ipv6.sh

echo "__CREATING SCRIPT TO ENABLE IPv6__"
echo "# Disable
# sysctl -w net.ipv6.conf.all.disable_ipv6=1
# sysctl -w net.ipv6.conf.default.disable_ipv6=1

# Enable
sysctl -w net.ipv6.conf.all.disable_ipv6=0
sysctl -w net.ipv6.conf.default.disable_ipv6=0" > /root/enable_ipv6.sh

echo "__MAKING ENABLE IPV6 SCRIPT__"
chmod u+x /root/enable_ipv6.sh

echo "__BACKUP PASSWD FILE__"
cp /etc/passwd /etc/passwd.bak

echo "__SAVE USERS TO FILE__"
cat /etc/passwd | grep -Ev "nologin|root|sync|shutdown|halt" | awk -F: '{ print $1 }' > users.list

echo "__LOCKING USER ACCOUNTS__"
cat users.list | while read line
do
passwd -l $line
done

echo "__UPDATE SYSTEM__"
yum update -y
yum upgrade -y

echo "__COMPLETE__"